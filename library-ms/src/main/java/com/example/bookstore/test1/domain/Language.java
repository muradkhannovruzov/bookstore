package com.example.bookstore.test1.domain;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = Language.TABLE_NAME)
public class Language {
    public static final String TABLE_NAME = "languages";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, nullable = false)
    private String shortName;
}
