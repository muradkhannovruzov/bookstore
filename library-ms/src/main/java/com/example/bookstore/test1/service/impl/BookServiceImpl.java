package com.example.bookstore.test1.service.impl;

import com.example.bookstore.test1.domain.Book;
import com.example.bookstore.test1.dto.BookDto;
import com.example.bookstore.test1.repository.BookRepository;
import com.example.bookstore.test1.service.BookService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final ModelMapper mapper;
    @Override
    public List<BookDto> getAll() {
        List<BookDto> bookDtoList = bookRepository.findAll()
                .stream().map(book-> mapper.map(book, BookDto.class))
                .collect(Collectors.toList());
        return  bookDtoList;
    }

    @Override
    public Book get(Long id) {
        return null;
    }
}
