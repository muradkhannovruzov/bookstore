package com.example.bookstore.test1.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = Author.TABLE_NAME)
public class Author {
    public static final String TABLE_NAME = "authors";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
}
