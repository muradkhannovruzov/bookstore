package com.example.bookstore.test1.config;




import com.example.bookstore.common.security.auth.services.JwtService;
import com.example.bookstore.common.security.auth.services.TokenAuthService;
import groovy.util.logging.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Slf4j
@Configuration
public class TokenAuthConfiguration {

    @Bean
    public TokenAuthService tokenAuthService(JwtService jwtService) {
        return new TokenAuthService(jwtService);
    }

}
