package com.example.bookstore.test1.service;

import com.example.bookstore.test1.domain.Book;
import com.example.bookstore.test1.dto.BookDto;

import java.util.List;

public interface BookService {
    List<BookDto> getAll();
    Book get(Long id);
}
