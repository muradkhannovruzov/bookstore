package com.example.bookstore.test1.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = Publisher.TABLE_NAME)
public class Publisher {
    public static final String TABLE_NAME="publishers";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    private String name;
}
