package com.example.common.dto.user;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.validation.constraints.NotNull;


@Data
public class AuthorityDto implements GrantedAuthority {
    @NotNull
    private Long id;

    @NotNull
    private String authority;
}
