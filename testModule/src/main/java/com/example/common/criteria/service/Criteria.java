package com.example.common.criteria.service;

public interface Criteria {

    Criteria copy();

}
