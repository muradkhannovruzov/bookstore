package com.example.bookstore.users.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class LoginDto {
    @NotNull
    @Size(min = 3)
    private String username;

    @NotNull
    @Size(min = 1, max = 30)
    private String password;

}
