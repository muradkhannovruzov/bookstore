package com.example.bookstore.users.controller;

import com.example.bookstore.common.security.auth.services.JwtService;
import com.example.bookstore.users.dto.LoginDto;
import com.example.bookstore.users.dto.SignUpDto;
import com.example.bookstore.users.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.net.ssl.SSLEngineResult;
import java.time.Duration;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/auth")
public class AuthController {
    private static final Duration ONE_DAY = Duration.ofDays(1);
    private final UserService userService;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JwtService jwtService;

    @PostMapping("sign-up")
    public ResponseEntity<Void> signUp(@RequestBody @Validated SignUpDto dto){
        log.trace("Sign up: ", dto.getUsername());
        userService.signUp(dto);
        return ResponseEntity.ok().build();
    }

    @PostMapping("sign-in")
    public ResponseEntity<AccessTokenDto> authorize(@RequestBody @Validated LoginDto loginDto){
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(loginDto.getUsername(),
                loginDto.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        Duration duration = ONE_DAY;
        String jwt = jwtService.issueToken(authentication, duration);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + jwt);
        return new ResponseEntity<>(new AccessTokenDto(jwt), httpHeaders, HttpStatus.OK);
    }
}
