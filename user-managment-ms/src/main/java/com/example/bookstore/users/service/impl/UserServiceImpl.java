package com.example.bookstore.users.service.impl;

import com.example.bookstore.users.domain.User;
import com.example.bookstore.users.dto.SignUpDto;
import com.example.bookstore.users.repository.UserRepository;
import com.example.bookstore.users.service.UserService;

import com.example.bookstore.users.service.errors.EmailAlreadyUsedException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void signUp(SignUpDto dto) {
        userRepository.findByUsername(dto.getUsername())
                .ifPresent(user -> {
                    throw new EmailAlreadyUsedException(dto.getUsername());
                });
        User user = createUserEntityObject(dto);
        userRepository.save(user);
    }

//    public void deleteUser(Long id){
//        User user = userRepository.findById(id)
//                .orElseThrow(()->new ApplicationException(Errors.USER_NOT_FOUND));
//    }

    private  User createUserEntityObject(SignUpDto dto){
        User user = modelMapper.map(dto, User.class);
        user.setName(dto.getName());
        user.setSurname(dto.getSurname());
        user.setUsername(dto.getUsername());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setCreationDate(LocalDateTime.now());
        user.setEnabled(true);
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        return user;
    }
}
