package com.example.bookstore.users.service.providers;

import com.example.bookstore.common.security.auth.services.ClaimSet;
import com.example.bookstore.common.security.auth.services.ClaimSetProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

import static com.example.bookstore.common.security.auth.services.TokenAuthService.ROLES_CLAIM;

@Slf4j
@Component
public class RuleAuthorityClaimSetProvider implements ClaimSetProvider {
    @Override
    public ClaimSet provide(Authentication authentication) {
        log.trace("Providing claims");
        System.out.println("Providing claims");
        Set<String> authorities = authentication.getAuthorities().stream()
                .map(a -> a.getAuthority())
                .collect(Collectors.toSet());
        return new ClaimSet(ROLES_CLAIM, authorities);
    }
}
