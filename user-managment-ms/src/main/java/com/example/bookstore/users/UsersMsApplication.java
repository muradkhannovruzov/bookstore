package com.example.bookstore.users;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@ComponentScan(basePackages = {"com.example.common", "com.example.bookstore.users", "com.example.bookstore.common.security"})
public class UsersMsApplication {

    public static void main(String[] args) {
            SpringApplication.run(UsersMsApplication.class, args);
    }

}
