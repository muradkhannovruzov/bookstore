package com.example.bookstore.users.service;

import com.example.bookstore.users.dto.SignUpDto;

public interface UserService {
    void signUp(SignUpDto dto);
}
