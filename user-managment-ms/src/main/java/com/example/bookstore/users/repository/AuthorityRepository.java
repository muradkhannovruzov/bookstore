package com.example.bookstore.users.repository;

import com.example.bookstore.users.domain.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
}
