package com.example.bookstore.common.security;

public enum UserRole {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_PUBLISHER
}
