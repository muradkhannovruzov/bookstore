package com.example.bookstore.common.security.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

public interface ApplicationSecurityConfigurationAdapter {
    void configure(HttpSecurity http);
}
